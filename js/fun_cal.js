$(document).ready(function() {

    $('#c-a').click(function() {
        $('#campoCalculador').val("");
        $('#cantidad1').text("")

    });

    $('#c').click(function() {
        $('#campoCalculador').val("");
    });

    crearCadena('cero', 0);
    crearCadena('uno', 1);
    crearCadena('dos', 2);
    crearCadena('tres', 3);
    crearCadena('cuatro', 4);
    crearCadena('cinco', 5);
    crearCadena('seis', 6);
    crearCadena('siete', 7);
    crearCadena('ocho', 8);
    crearCadena('nueve', 9);
    crearCadena('punto', '.');

    crearOperacion('barra_div', '/');
    crearOperacion('multiplicar', '*');
    crearOperacion('mas', '+');
    crearOperacion('restar', '-');

});



function crearCadena(control, valor) {
    $('#' + control).click(function() {
        var valActual = $('#campoCalculador').val();
        cadena = valActual + valor;
        $('#campoCalculador').val(cadena);
    });
}



function crearOperacion(control, operacion) {
    $('#' + control).click(function() {
        valorActual = $('#campoCalculador').val();
        $('#cantidad1').text(operacion + " " + valorActual);
        $('#campoCalculador').val("");
    });
}



function operacionCompleta() {
    valorActual = $('#campoCalculador').val();
    cantidadGuardada = $('#cantidad1').text();
    tipo = cantidadGuardada.split(" ");

    switch (tipo[0]) {

        case '+':
            resultadototal = parseInt(valorActual) + parseInt(tipo[1]);
            $('#campoCalculador').val(resultadototal);
            break;

        case '-':
            resultadototal = parseInt(valorActual) - parseInt(tipo[1]);
            $('#campoCalculador').val(resultadototal);
            break;

        case '*':
            resultadototal = parseInt(valorActual) * parseInt(tipo[1]);
            $('#campoCalculador').val(resultadototal);
            break;

        case '/':
            resultadototal = parseInt(valorActual) / parseInt(tipo[1]);
            $('#campoCalculador').val(resultadototal);
            break;
    }
}






/*Confilmar si es un númro primo*/

function numeroPrimo(np) {
    if (np != 1) {
        for (var i = 2; i < np; i++)
            if (np % i == 0) return false;
        return np !== 1;
    }
}

function confilmarPrimo() {
    var np = document.getElementById("name").value;
    var estenp = "";

    if (!isNaN(np)) {
        if (numeroPrimo(np)) {
            estenp = "El númro " + (np) + " es primo";
        } else {
            estenp = "El númro " + (np) + " no es primo";
        }
        document.getElementById("mensaje").innerHTML = estenp;
    } else {
        document.getElementById("mensaje").innerHTML = "El número es";
    }
}